import './Topic.css';
function Topic() {
    return(
        <div id="TopicBG">
            <h3>ดูวิดีโอแบบไม่มีโฆษณา ขณะใช้แอปอื่นๆ หรือเมื่อล็อกหน้าจอ</h3>
            <hr/>
        </div>
    );
}
export default Topic;